#!/usr/bin/env nextflow
nextflow.enable.dsl = 2

import java.nio.file.Paths

def project_dir = Paths.get("").toAbsolutePath().toString()
println "Current Directory: $project_dir"

def targetName="AF003887|AF003887"
def targetLength=9739
def gapAlignment=100
def distanceToLTR=100
def returnFilteredOut=true
def overlap=15
def depth=1
def spaceBetweenIS=1000
def referenceTable="${project_dir}/GRCh38_circos.csv"

println ("Reference: $referenceTable")


println "Please enter the PAF file name for analysis (absolute path): "
def PAF = System.console().readLine()

def choice=99

while (choice!=0) {
    println("Here are the current parameters:")
    println("1. PAF file to analyse: "+PAF)
    println("2. Target name: "+targetName+"\t3. Target length: "+targetLength)
    println("4. Authorized gap: "+gapAlignment+"\t5. Authorized distance to LTR: "+distanceToLTR+"\t6. Authorized overlap: "+overlap+"\t7. Minimum depth: "+depth)
    println("8. Distance between integration sites to be grouped together: "+spaceBetweenIS)
    println("9. Creation of a file with target reads having both ends over distance to LTR: "+returnFilteredOut)
    println("10. Reference file for graphic visualisation: "+referenceTable)
    println("Press the number corresponding to a parameter to change it or press 0 to launch the analysis.")
    choice=System.console().readLine().toInteger()
    switch (choice) {
        case 1:
            println ("Please chose the absolute path to the PAF file:")
            PAF=System.console().readLine()
            break
        case 2:
            println ("Please enter the target name:")
            targetName=System.console().readLine()
            break
        case 3:
            println ("Please enter the target length:")
            targetLength=System.console().readLine().toInteger()
            break
        case 4:
            println ("Please enter the authorized gap between host and target on the reads:")
            gapAlignment=System.console().readLine().toInteger()
            break
        case 5:
            println ("Please enter the authorized distance to the LTR:")
            distanceToLTR=System.console().readLine().toInteger()
            break
        case 6:
            println ("Please enter the authorized overlap between host and target on the reads:")
            overlap=System.console().readLine().toInteger()
            break
        case 7:
            println ("Please enter the minimum depth to keep an integration site:")
            depth=System.console().readLine().toInteger()
            break
        case 8:
            println ("Please enter the distance between integration sites to be grouped together:")
            spaceBetweenIS=System.console().readLine().toInteger()
            break
        case 9:
            if (returnFilteredOut==true) {
                returnFilteredOut=false
            }
            else {
                returnFilteredOut=true
            }
            println("The parameter has been changed")
            break
        case 10:
            println ("Please enter the absolute path to the file to build the reference for the graphic visualisation of the results:")
            referenceTable=System.console().readLine()
            break
        case 0:
            def command = "python3 ${project_dir}/analysing.py ${PAF} \"${targetName}\" ${targetLength} ${gapAlignment} ${distanceToLTR} ${returnFilteredOut} ${overlap} ${depth} ${spaceBetweenIS} ${referenceTable}"
            println(command)
            ProcessBuilder processBuilder = new ProcessBuilder()
            processBuilder.command("bash", "-c", command)

            processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT)

            Process process = processBuilder.start()

            int exitCode = process.waitFor()

            if (exitCode == 0) {
                println "The analysis has been succesfully fulfilled."
            } else {
                println "Analysis failed: $exitCode"
            }
            break
        default:
            println("Invalid choice")
            break   
    }
}
