import pandas as pd
import os
import sys
import pycircos
Garc    = pycircos.Garc
Gcircle = pycircos.Gcircle
import collections


def filter(PAF,targetName):
    ''' Filters reads in order to keep only those with beginning in the target and continuing in the host
    @param PAF: alignement PAF file
    @param targetName: name of the target as in the index/fasta'''
    file=pd.read_table(PAF,header=None,dtype={1:int,2:int,3:int,6:int,7:int,8:int},names=range(24))
    # Removes non primary reads
    lineToKeep=[]
    for i in range(file.shape[0]):
        if file.iloc[i,16]=="tp:A:P":
            lineToKeep.append(i)
    file=file.iloc[lineToKeep]
    file=file.sort_values(by=[0,2])
    file=file.reset_index(drop=True)
    # Determines read composition per strand (1=target/provirus 0=host)
    # Reads beginning with 10 will be kept
    currentLine=0
    filteredLines=[]
    while currentLine<file.shape[0]:
        newReadName=file.iloc[currentLine,0]
        newReadLines=[]
        currentReadName=newReadName
        readType=""
        keep=False  # If keep becomes True, saves the first two lines from the read (target and adjacent host)
        dump=False  # If dump becomes True, read is none of expected. The search goes on to the next read.
        while currentReadName==newReadName and currentLine<file.shape[0]:
            newReadLines.append(currentLine)
            if file.iloc[currentLine,5]==targetName and readType == "":
                readType+="1"
            elif file.iloc[currentLine,5]!=targetName and readType == "1":
                readType+="0"
                keep=True
            else:
                dump=True
            if dump==True or keep==True:  # Looping to next read
                while currentReadName==newReadName and currentLine<file.shape[0]:
                    currentLine+=1
                    if currentLine<file.shape[0]:
                        currentReadName=file.iloc[currentLine,0]
                break      
            else:
                currentLine+=1
                if currentLine<file.shape[0]:
                    currentReadName=file.iloc[currentLine,0]
        if dump==True:
            continue
        if keep==True:
            for line in newReadLines:
                filteredLines.append(line)
    file=file.iloc[filteredLines]
    file=file.sort_values(by=[0,2])
    saveFile=PAF[0:-4]+"_filtered.paf"
    file.to_csv(saveFile,sep='\t',index=False,header=False)
    return saveFile

def breakpoints(PAF=None,targetName=None,targetLenth=None, gapAlignment=100, distanceToLTR=100, returnFilteredOut=True, overlap=15):
    ''' Extract TARGET-HOST junctions (=breakpoints)
    @param PAF: path to the PAF file to be analyzed
    @param targetLenth: length (in base pairs) of the sequence targeted by the PCIP seq primers
    @param targetName: name of the sequence targeted by the PCIP seq primers (use the exact name in the index)
    @param gapAlignment: maximum authorized base pairs between host and target sequences
    @param distanceToLTR: maximum authorized base pairs between the ends of a read in the target sequence and the 5' or 3' edges
    @param returnFilteredOut: if True, will create a PAF file containing the reads mapped in the target but with distanceToLTR above the chosen value
    @param overlap: number of bases that can overlap between target and host alignment'''
    if PAF==None:
        return ValueError("Please insert the path to the PAF file as first parameter")
    if targetLenth==None:
        return ValueError("Please insert the length of the target chromosome/provirus in base pairs as third parameter")
    if targetName==None:
        return ValueError("Please provide target chromosome/provirus name as second parameter")
    file=pd.read_table(PAF,header=None,dtype={1:int,2:int,3:int,6:int,7:int,8:int},names=range(24))
    file=file.sort_values(by=[0,2])
    file=file.reset_index(drop=True)
    if returnFilteredOut==True:
        sortedOutReads=[]
    integrationSites=pd.DataFrame(columns=['Read ID','Read Length','Read Orientation','Sequence Name','Sequence Length','Begin IS','End IS','Gap','LTR'])
    # Selecting all the read with an integration site and stores them in a PAF file with extension _reads_with_IS
    significantReads=[]
    currentLine=0
    while currentLine<file.shape[0]:
        newReadName=file.iloc[currentLine,0]
        readsTarget5prime=[]
        readsTarget3prime=[]
        readsHost=[]
        readsTargetNeither5Or3=[]
        currentReadName=newReadName
        while currentReadName==newReadName:
            if file.iloc[currentLine,5]==targetName:
                # Determining if portion of read mapped in target is close to an edge depending of parameter distanceToLTR and targetLenth
                if file.iloc[currentLine,7]<=distanceToLTR and file.iloc[currentLine,7]<targetLenth-file.iloc[currentLine,8]:
                    readsTarget5prime.append(currentLine)
                elif targetLenth-file.iloc[currentLine,8]<=distanceToLTR:
                    readsTarget3prime.append(currentLine)
                else:
                    readsTargetNeither5Or3.append(currentLine)
            else:
                readsHost.append(currentLine)
            currentLine+=1
            if currentLine<file.shape[0]:
                currentReadName=file.iloc[currentLine,0]
            else:
                break
        # Sorting reads that should have had an integration site but have not. Parameter returnFilteredOut must be set on True.
        if readsTargetNeither5Or3!=[] and returnFilteredOut==True:
            sortedOutReads.append(readsTargetNeither5Or3[0])
        # Comparing portion of the read mapped in target and adjacent portion of the read mapped in host to check if gap is <= gapAlignment parameter and >= -overlap
        # depending on the edge (5' or 3') 
        elif readsTarget5prime!=[]:
            for targetRead in readsTarget5prime:
                for hostRead in readsHost:
                    if abs(file.iloc[hostRead,2]-file.iloc[targetRead,3])<=gapAlignment and file.iloc[hostRead,2]-file.iloc[targetRead,3]>=-overlap:
                        if file.iloc[hostRead,4]==file.iloc[targetRead,4]:
                            df=pd.DataFrame([[file.iloc[targetRead,0],file.iloc[targetRead,1],'-',file.iloc[hostRead,5],file.iloc[hostRead,6], \
                                                min(file.iloc[hostRead,8], file.iloc[hostRead,8]+file.iloc[hostRead,2]-file.iloc[targetRead,3]), \
                                                max(file.iloc[hostRead,8], file.iloc[hostRead,8]+file.iloc[hostRead,2]-file.iloc[targetRead,3]), \
                                                file.iloc[hostRead,2]-file.iloc[targetRead,3],"5'"]], \
                                                columns=['Read ID','Read Length','Read Orientation','Sequence Name','Sequence Length','Begin IS','End IS','Gap','LTR'])
                        else:
                            df=pd.DataFrame([[file.iloc[targetRead,0],file.iloc[targetRead,1],'+',file.iloc[hostRead,5],file.iloc[hostRead,6], \
                                            min(file.iloc[hostRead,7]-file.iloc[hostRead,2]+file.iloc[targetRead,3],file.iloc[hostRead,7]), \
                                            max(file.iloc[hostRead,7]-file.iloc[hostRead,2]+file.iloc[targetRead,3],file.iloc[hostRead,7]), \
                                            file.iloc[hostRead,2]-file.iloc[targetRead,3],"3'"]], \
                                            columns=['Read ID','Read Length','Read Orientation','Sequence Name','Sequence Length','Begin IS','End IS','Gap','LTR'])
                        integrationSites=pd.concat([integrationSites,df],ignore_index=True)
                        significantReads.append(hostRead)
                        significantReads.append(targetRead)                            
        elif readsTarget3prime!=[]:
            for targetRead in readsTarget3prime:
                for hostRead in readsHost:
                    if file.iloc[hostRead,2]-file.iloc[targetRead,3]<=gapAlignment and file.iloc[hostRead,2]-file.iloc[targetRead,3]>=-overlap:
                        if file.iloc[hostRead,4]==file.iloc[targetRead,4]:
                            df=pd.DataFrame([[file.iloc[targetRead,0],file.iloc[targetRead,1],'+',file.iloc[hostRead,5],file.iloc[hostRead,6], \
                                                min(file.iloc[hostRead,7]-file.iloc[hostRead,2]+file.iloc[targetRead,3],file.iloc[hostRead,7]), \
                                                max(file.iloc[hostRead,7]-file.iloc[hostRead,2]+file.iloc[targetRead,3],file.iloc[hostRead,7]), \
                                                file.iloc[hostRead,2]-file.iloc[targetRead,3],"3'"]], \
                                                columns=['Read ID','Read Length','Read Orientation','Sequence Name','Sequence Length','Begin IS','End IS','Gap','LTR'])
                        else:
                            df=pd.DataFrame([[file.iloc[targetRead,0],file.iloc[targetRead,1],'-',file.iloc[hostRead,5],file.iloc[hostRead,6], \
                                                min(file.iloc[hostRead,8], file.iloc[hostRead,8]+file.iloc[hostRead,2]-file.iloc[targetRead,3]), \
                                                max(file.iloc[hostRead,8], file.iloc[hostRead,8]+file.iloc[hostRead,2]-file.iloc[targetRead,3]), \
                                                file.iloc[hostRead,2]-file.iloc[targetRead,3],"5'"]], \
                                                columns=['Read ID','Read Length','Read Orientation','Sequence Name','Sequence Length','Begin IS','End IS','Gap','LTR'])
                        integrationSites=pd.concat([integrationSites,df],ignore_index=True)
                        significantReads.append(hostRead)
                        significantReads.append(targetRead)                        
    # Saving file containing reads with integration sites
    saveFile1=PAF[0:-4]+"_IS.csv"
    integrationSites=integrationSites.sort_values(["Sequence Name","Begin IS"])
    integrationSites.to_csv(saveFile1,index=False)
    # Saving file with sorted out reads if option is chosen
    if returnFilteredOut==True:
        saveFile=PAF[0:-4]+"_sortedOut.paf"
        if sortedOutReads!=[]:
            dfSortedOut=file.iloc[sortedOutReads]
            dfSortedOut=dfSortedOut.sort_values(by=[0,4,2])
            dfSortedOut.to_csv(saveFile,sep='\t',index=False,header=False)           
        else:
            message=f"No read with distance to each edge above {distanceToLTR} bp"
            with open(saveFile, 'w') as fileToSave:
                fileToSave.write(message)
        
    # Saving PAF file with all the reads having an integration site
    saveFile2=PAF[0:-4]+"_reads_with_IS.paf"
    file=file.iloc[significantReads]
    file=file.sort_values(by=[0,4,2])
    file=file.reset_index(drop=True)
    file.to_csv(saveFile2,sep='\t',index=False,header=False)

    # Saving csv file with parameters
    saveFile=PAF[0:-4]+"_parameters.csv"
    message="Analysed file:\t"+PAF+"\nTarget name:\t"+targetName+"\nTarget length:\t"+str(targetLenth)+"\nAllowed gap:\t"+str(gapAlignment)+"\nAllowed distance to LTR:\t"+str(distanceToLTR)+ \
    "\nAllowed overlap:\t"+str(overlap)+"\nFiltering non-matching target reads:\t"+str(returnFilteredOut)+"\n"
    with open(saveFile,"w") as fileToSave:
        fileToSave.write(message)
    return saveFile1,saveFile2

def summarise(CSV,PAF,depth=10,spaceBetweenIS=1000):
    '''Transforms the CSV file with the integration sites in two tables easier to interpret and a directory with PAF files
    @param CSV: CSV file generated by the PCIP_breakpoints function (name_IS.csv)
    @param depth: minimum depth to select an integration site in the summary
    @param spaceBetweenIS: maximum number of bases between two integration sites to group them together
    First table= name_IS_summary.csv reports all the integration sites with sufficient depth
    Second table= gathers the integrations site located within pair bases distance given by spaceBetweenIS
    Directory=a paf file is created for each integration site (on forward and reverse strands together) with all the reads mapped'''
    file=pd.read_csv(CSV)
    file=file.sort_values("Gap", key=lambda x: abs(x))
    file=file.reset_index(drop=True)
    # Summarising integrations sites depending on the chosen depth
    summary={}
    for i in range (file.shape[0]):
        key=file.iloc[i,3]
        length=file.iloc[i,4]
        strand=file.iloc[i,2]
        beginIS=file.iloc[i,5]
        endIS=file.iloc[i,6]
        gap=file.iloc[i,7]
        LTR=file.iloc[i,8]
        readName=file.iloc[i,0]
        if key not in summary:
            summary[key]=[[[beginIS,endIS],[gap],LTR,strand,length,[readName]]]
        else:
            knownIS=False
            for j in range(len(summary[key])):
                mapped=False
                if beginIS>summary[key][j][0][0] and beginIS<=summary[key][j][0][1] and strand==summary[key][j][3] and LTR==summary[key][j][2]:
                    summary[key][j][0][0]=beginIS
                    mapped=True
                if endIS<summary[key][j][0][1] and endIS>=summary[key][j][0][0] and strand==summary[key][j][3] and LTR==summary[key][j][2]:
                    summary[key][j][0][1]=endIS
                    mapped=True
                if beginIS<=summary[key][j][0][0] and endIS>=summary[key][j][0][1] and strand==summary[key][j][3] and LTR==summary[key][j][2]:
                    mapped=True
                if mapped==True:
                    summary[key][j][1].append(gap)
                    summary[key][j][5].append(readName)
                    knownIS=True
            if knownIS==False:
                summary[key].append([[beginIS,endIS],[gap],LTR,strand,length,[readName]])
    df=pd.DataFrame(columns=["Seq name","Seq length","Strand","Begin IS","End IS","Reads count","Gap=0","Gap 1-10","Gap 11-20","Gap >20","Overlap","LTR","Read name"])
    for seq in summary.keys():
        for IS in summary[seq]:
            if len(IS[1])>=depth:
                Gap0,Gap1_10,Gap11_20,Gap20,overlap=0,0,0,0,0
                for i in range(len(IS[1])):
                    if IS[1][i]<0:
                        overlap+=1
                    elif IS[1][i]==0:
                        Gap0+=1
                    elif IS[1][i]>0 and IS[1][i]<11:
                        Gap1_10+=1
                    elif IS[1][i]>10 and IS[1][i]<21:
                        Gap11_20+=1
                    elif IS[1][i]>20:
                        Gap20+=1 
                dftemp=pd.DataFrame([[seq,IS[4],IS[3],IS[0][0],IS[0][1],len(IS[1]),Gap0,Gap1_10,Gap11_20,Gap20,overlap,IS[2],IS[5]]], \
                                    columns=["Seq name","Seq length","Strand","Begin IS","End IS","Reads count","Gap=0","Gap 1-10","Gap 11-20","Gap >20","Overlap","LTR","Read name"])
                df=pd.concat([df,dftemp],ignore_index=True)
    
    
    df=df.sort_values(["Seq name","Strand","Begin IS","LTR"])
    df=df.reset_index(drop=True) 

    # Compacting the integration site depending on the chosen distance bewteen two adjacent integration sites (spaceBetweenIS) 
    dfcompact=pd.DataFrame(columns=["Seq name","Seq length","Strand","Begin IS","End IS","Best IS begin","Best IS end","Best reads count","LTR","NCBI link","Read name"])
    count=1
    indexIS=[]
    currentLine=0
    while currentLine<df.shape[0]:
        compactBeginIS=df.iloc[currentLine,3]
        compactEndIS=df.iloc[currentLine,4]
        compactSeqName=df.iloc[currentLine,0]
        compactLength=df.iloc[currentLine,1]
        compactLTR=df.iloc[currentLine,11]
        compactStrand=df.iloc[currentLine,2]
        compactReads=df.iloc[currentLine,12]
        bestReadCount=df.iloc[currentLine,5]
        bestBegin=compactBeginIS
        bestEnd=compactEndIS
        if currentLine!=df.shape[0]-1:
            currentLine+=1
            indexIS.append(count)
        while df.iloc[currentLine,0]==compactSeqName and df.iloc[currentLine,2]==compactStrand and df.iloc[currentLine,11]==compactLTR and abs(df.iloc[currentLine,3]-compactBeginIS)<=spaceBetweenIS:
            if df.iloc[currentLine,4]>compactEndIS:
                compactEndIS=df.iloc[currentLine,4]
            if df.iloc[currentLine,5]>bestReadCount:
                bestReadCount=df.iloc[currentLine,5]
                bestBegin=df.iloc[currentLine,3]
                bestEnd=df.iloc[currentLine,4]
            for read in df.iloc[currentLine,12]:
                if read not in compactReads:
                    compactReads.append(read)
            currentLine+=1
            indexIS.append(count)
            if currentLine==df.shape[0]:
                break
        count+=1
        linkNCBI="https://www.ncbi.nlm.nih.gov/nuccore/"+compactSeqName+"?report=graph&from="+str(bestBegin)+"&to="+str(bestEnd)
        dftemp=pd.DataFrame([[compactSeqName,compactLength,compactStrand,compactBeginIS,compactEndIS,bestBegin,bestEnd,bestReadCount,compactLTR,linkNCBI,compactReads]], \
                            columns=["Seq name","Seq length","Strand","Begin IS","End IS","Best IS begin","Best IS end","Best reads count","LTR","NCBI link","Read name"])
        dfcompact=pd.concat([dfcompact,dftemp],ignore_index=True)
    

    # Saving compact version of the summary
    saveFile1=CSV[0:-4]+"_summary_compact.csv"
    dfcompact=dfcompact.sort_values(["Seq name","Begin IS"])
    dfcompact=dfcompact.reset_index(drop=True)
    dfcompact.index=dfcompact.index+1
    dfcompact[["Seq name","Seq length","Strand","Begin IS","End IS","Best IS begin","Best IS end","Best reads count","LTR","NCBI link"]].to_csv(saveFile1,index=True)

    # Saving integration sites summary with index corresponding to the compact integration site 
    df=df.sort_values(["Seq name","Begin IS"])
    saveFile=CSV[0:-4]+"_summary.csv"
    df["IS index"]=indexIS
    df[["Seq name","Seq length","Strand","Begin IS","End IS","Reads count","Gap=0","Gap 1-10","Gap 11-20","Gap >20","Overlap","LTR","IS index"]].to_csv(saveFile,index=False)

    # Creating a PAF file per compacted integration site
    readsWithISFile=pd.read_table(PAF,header=None,dtype={1:int,2:int,3:int,6:int,7:int,8:int},names=range(24))
    newDirectory=CSV[0:-7]+"_reads_per_IS/"
    os.makedirs(newDirectory)
    flag=0
    for i in range (1,len(CSV)):
        if CSV[-i]=="/" or CSV[-i]=="\\":
            flag=i
            break
    fileName=CSV[-flag+1:-7]
    currentLine=0
    while currentLine<dfcompact.shape[0]:
        if currentLine<dfcompact.shape[0]-2 and dfcompact.iloc[currentLine,0]==dfcompact.iloc[currentLine+1,0] and abs(dfcompact.iloc[currentLine,3]-dfcompact.iloc[currentLine+1,3])<=spaceBetweenIS:
            totalReads=dfcompact.iloc[currentLine,10]
            for read in dfcompact.iloc[currentLine+1,10]:
                if read not in totalReads:
                    totalReads.append(read)
            lineWithMatchingRead=[]
            for i in range(readsWithISFile.shape[0]):
                if readsWithISFile.iloc[i,0] in totalReads:
                    lineWithMatchingRead.append(i)
            dftemp=readsWithISFile.iloc[lineWithMatchingRead]
            saveFile=CSV[0:-7]+"_reads_per_IS/"+fileName+"_"+str(dfcompact.iloc[currentLine,0])+"_"+str(dfcompact.iloc[currentLine,5])+"_"+str(dfcompact.iloc[currentLine,6])+".paf"
            dftemp.to_csv(saveFile,sep='\t',index=False)
            if currentLine<dfcompact.shape[0]-2:
                currentLine+=2
            else:
                currentLine=dfcompact.shape[0]
        else:
            totalReads=dfcompact.iloc[currentLine,10]
            lineWithMatchingRead=[]
            for i in range(readsWithISFile.shape[0]):
                if readsWithISFile.iloc[i,0] in totalReads:
                    lineWithMatchingRead.append(i)
            dftemp=readsWithISFile.iloc[lineWithMatchingRead]
            saveFile=CSV[0:-7]+"_reads_per_IS/"+fileName+"_"+str(dfcompact.iloc[currentLine,0])+"_"+str(dfcompact.iloc[currentLine,5])+"_"+str(dfcompact.iloc[currentLine,6])+".paf"
            dftemp.to_csv(saveFile,sep='\t',index=False,header=False)
            if currentLine<dfcompact.shape[0]-1:
                currentLine+=1
            else:
                currentLine=dfcompact.shape[0]

    # Saving csv file with parameters
    saveFile=CSV[0:-7]+"_parameters.csv"
    message="Minimum depth:\t"+str(depth)+"\nDistance to group insertion sites:\t"+str(spaceBetweenIS)
    with open(saveFile,"a") as fileToSave:
        fileToSave.write(message)

    return saveFile1

def visualise(referenceTable,compact):
    ''' Visualises the integration sites from the compact summary file on the chromosomes
    @param referenceTable: a csv file with the name of the sequence, the begin (value of 1) and the end (value of the length of the sequence)
    @param compact: compact summary csv file obtained with the summarise fonction'''
    circle=Gcircle(figsize=(32,32))
    with open(referenceTable) as f:
        f.readline()
        count=0
        for line in f:
            line=line.rstrip().split(",")
            name=line[0]
            length=int(line[-1])
            if count%2==0:
                arc=Garc(arc_id=name, size=length, interspace=2,raxis_range=(800,850),labelposition=70,label_visible=True)
            else:
                arc=Garc(arc_id=name, size=length, interspace=2,raxis_range=(800,850),labelposition=100,label_visible=True)
            circle.add_garc(arc)
            count+=1
    circle.set_garcs(-85,265)
    for label in circle.ax.texts:
        label.set_fontsize(32)
    for arc_id in circle.garc_dict:
        circle.tickplot(arc_id, raxis_range=(850,870), tickinterval=20000000, ticklabels=None)
    circle.figure
    arcdata_dict = collections.defaultdict(dict)
    with open(compact) as f:
        f.readline()
        for line in f:
            line  = line.rstrip().split(",")
            name  = line[1]     
            start = int(line[6]) 
            if int(line[2])-(int(line[6])-1)>1000000:
                width=1000000
            else:
                width = int(line[2])-(int(line[6])-1) 
            if name not in arcdata_dict:
                arcdata_dict[name]["positions"] = []
                arcdata_dict[name]["widths"]    = [] 
            arcdata_dict[name]["positions"].append(start) 
            arcdata_dict[name]["widths"].append(width)
    for key in arcdata_dict:
        if key in circle._garc_dict:
            circle.barplot(key, data=[1]*len(arcdata_dict[key]["positions"]), positions=arcdata_dict[key]["positions"], \
                        width=arcdata_dict[key]["widths"], raxis_range=[800,850], facecolor="#000000")
    circle.figure
    saveFile=compact[0:-4]+".png"
    circle.figure.savefig(saveFile)

PAF = sys.argv[1]
targetName = sys.argv[2]
targetLength = int(sys.argv[3])
gapAlignment = int(sys.argv[4])
distanceToLTR = int(sys.argv[5])
returnFilteredOut=bool(sys.argv[6])
overlap=int(sys.argv[7])
depth=int(sys.argv[8])
spaceBetweenIS=int(sys.argv[9])
referenceTable=sys.argv[10]

filteredPAF=filter(PAF,targetName)
csvIS,pafIS=breakpoints(filteredPAF,targetName,targetLength,gapAlignment,distanceToLTR,returnFilteredOut,overlap)
csvCompact=summarise(csvIS,pafIS,depth,spaceBetweenIS)
visualise(referenceTable,csvCompact)

