#!/bin/bash

while true
do

echo "What would you like to do?"
echo "1. Create and index a chimeric genome"
echo "2. Map a fastq to a reference genome"
echo "3. Analyse results from a PAF file"
echo "4. Exit program"
echo

read -p "Please enter your choice (1,2,3 or 4): " choice
echo

case $choice in
    1)
        echo "You have chosen to create and index a chimeric genome."
        ./nextflow run indexing.nf
        ;;
    2)
        echo "You have chosen to map a fastq to a reference genome."
        ./nextflow run mapping.nf
        ;;
    3)
	echo "You have chosen to analyse the results from a PAF file."
	./nextflow run analysing.nf
	;;
    4)
	echo "Goodbye"
        exit 0
	;;
    *)
        echo "Invalid choice. Please enter 1,2,3 or 4."
        ;;
esac

done

exit 0
