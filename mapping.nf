#!/usr/bin/env nextflow
nextflow.enable.dsl = 2



println "Please enter the FASTQ file name (absolute path): "
fastq = System.console().readLine()
println "Please enter the index file name (absolute path ending with .mni): "
index = System.console().readLine()
println "Please enter the output PAF file name (absolute path ending with .paf): "
paf_name = System.console().readLine()


def command="minimap2 -I2g -cx map-ont -t 3 "+index+" "+fastq+" > "+paf_name

ProcessBuilder processBuilder = new ProcessBuilder()
processBuilder.command("bash", "-c", command)


processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT)


Process process = processBuilder.start()


int exitCode = process.waitFor()


if (exitCode == 0) {
    println "The mapping has been succesfully fulfiled in the file: $paf_name"
} else {
    println "Mapping failed: $exitCode"
}