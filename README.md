# NextFlow Pipeline
Project repository for Nextflow Pipeline of Oxford Nanopore Data Analysis.

## Table of contents
-   [Prerequisites](#prerequisites)
-   [Installation](#installation)
    -   [Linux](#linux)
    -   [Mac](#mac)
    -   [Windows](#windows)
-   [Running a Nextflow Workflow](#running-a-nextflow-workflow)
    -   [How to use the runpipeline.sh script](#how-to-use-the-runpipelinesh-script)
-   [Additional Software Insallation](#additional-software-insallation)
-   [Features](#features)
-   [Authors](#authors)


## Overview

This is a Nextflow pipeline designed to perform an analysis on HIV sequence files. The pipeline allows the user to customize various parameters including the filename, target sequence, target length, alignment gap, distance to LTR, file sorting option, alignment overlap, read coverage, and the gap between integration sites.


## Pipeline Processes

The pipeline includes the following processes:

- Creation an indexation of a chimeric genome
- Mapping of a fastq on a reference genome
- Analysis of the PAF file resulting from the mapping

Each process can be executed independantly.


## Prerequisites
* Unix-like OS (Linux, macOS, etc.)
* [Java](http://jdk.java.net/) 8 or later , up to 20
* Nextflow requires Bash 3.2 (or later) 
* Porechop (Optional)
* Minimape2
* Python3 - Some parts of this pipeline require Python3. Ensure it is installed and properly set up on your machine.
* Pandas

## Installation

### Linux

- Install Nextflow by running the following command in your terminal:
   ```bash
   curl -s https://get.nextflow.io | bash
   ```
### Mac

- [Install Xcode from the Mac App Store.](https://apps.apple.com/us/app/xcode/id497799835?mt=12)
- [Install Java.](https://www.java.com/en/download/) You can install Java using Homebrew by running the following command:
    ```bash
    brew install nextflow
    ```
### Windows
- Install the Unix subsystem
- Install Java 8 or above
    ```bash
    sudo apt get install openjdk-8-jdk


### NextFlow	Installation

**.1**  Download the executable package by copying and pasting either one of the following commands in your terminal window :

```
curl -s https://get.nextflow.io | bash
```

or :

```
wget -gO- https://get.nextflow.io | bash
```

This will create the nextflow main executable file in the current directory.

**2.**  Make the binary executable on your system by running

```
chmod +x nextflow
```

**3.**  Optionally, move the nextflow file to a directory accessible by your $PATH variable (this is only required to avoid remembering and typing the full path to nextflow each time you need to run it).

## Running a nextflow workflow

Nextflow has many commands but we are going to focus on the run, pull and drop commands.

To execute any of these commands, we type nextflow then the command.
| Command |Description |
| ------------- | -----------------------------------------------|
| clean | Clean up project cache and work directories |
| clone | Clone a project into a folder |
| config | Print a project configuration |
| console | Launch Nextflow interactive console |
| drop | Delete the local copy of a project |
| help | Print the usage help for a command |
| info | Print project and system runtime information |
| list | List all downloaded projects |
| log | Print executions log and runtime info |
| pull | Download or update a project |
| run | Execute a pipeline project |
| self-update | Update nextflow runtime to the latest available version |
| view | View project script file(s) |

**run:**
```
nextflow run main.nf --reads 'path/to/data/*' --outdir results
```
-   `-reads`: Path to input data (must be in quotes)
-   `-outdir`: Name for output directory
The asterisk (*) at the end of the path tells Nextflow to include all files in the directory. Be sure to change the path to point to your data files.

**pull:**
```
nextflow pull <repository_name>
```

**drop:**
```
nextflow drop <repository_name>
```

**view:**
```
nextflow view <repository_name>
```

**clean:**
```
nextflow clean
```

### How to use the runpipeline.sh script:

1. Open your terminal.
2. Navigate to the directory containing your pipeline.nf script.
    ```bash
    cd /path/to/your/script

3. To check if Nextflow is correctly installed, type the following command:
    ```bash
    nextflow
If Nextflow is installed correctly, you will see a message with the Nextflow help and usage informatio

4. To run the pipeline, use the following command:
    ```bash
    ./runpipeline.sh


This will launch the interactive menu for the pipeline. You'll then be prompted to enter various parameters that the script needs for its execution.

-   Note This script assumes that the Python scripts and the shell script are present in the same directory as the Nextflow script.

## Additional Software Installation
Additional software required for this pipeline include minimap2, Python, pandas for Python, and optionally porechop.

### Python3

Install python3 on Linux using the following command:
```
sudo apt install python3
```

Install python3 on Windows by follonwing the instruction on this site:
```
https://www.python.org/downloads/
```


### Minimape2
Minimap2 is a versatile sequence alignment program that aligns DNA or mRNA sequences against a large reference database.
- linux 
Install minimap2 on Linux using the following command:
```
sudo apt-get install minimap2
```
- Mac
Install minimap2 on Mac using the following command:
```
brew install minimap2
```

### Pandas
Pandas is a fast, powerful, flexible, and easy to use open source data analysis and manipulation tool, built on top of the Python programming language.
- Installation
Install pandas using pip:
```
pip install pandas
```
or with conda:
```
conda install pandas
```

### pyCircos
pyCircos is a python Matplotlib based circular genome visualization package

- Installation
Install pyCircos using pip:
```
pip install git+https://github.com/ponnhide/pyCircos.git
```
or:
```
Github: https://github.com/ponnhide/pyCircos
```
    
### Porechop (Optional)
Porechop is a tool for finding and removing adapters from Oxford Nanopore reads. Adapters on the ends of reads are trimmed off, and when a read has an adapter in its middle, it is treated as chimeric and chopped into separate reads.
- Installation
Porechop can be installed via conda:
```
conda install -c bioconda porechop
```

Or via pip:
```
pip install porechop
```

- Please note, installation instructions might be updated in the future, so always refer to the original documentation for each tool.


## Authors

- Sara AIT ABED [@saitabed](https://gitlab.emi.u-bordeaux.fr/saitabed)
- Maroa ALANI [@malani](https://gitlab.emi.u-bordeaux.fr/malani)
- Theo MOSER [@tmoser](https://gitlab.emi.u-bordeaux.fr/tmoser)
- Laurent Busson [@lbusson](https://gitlab.emi.u-bordeaux.fr/lbusson)
- Gaia BERKACHE [@gberkache](https://gitlab.emi.u-bordeaux.fr/gberkache)

## Acknowledgements

- We would like to acknowledge the following resources and libraries that were instrumental in the development of this pipeline:
