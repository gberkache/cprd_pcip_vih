#!/usr/bin/env nextflow
nextflow.enable.dsl = 2
import java.io.File
import java.io.FileWriter

void concatenateFiles(String filePath1, String filePath2, String outputFilePath) {
    try {
        File file1 = new File(filePath1)
        File file2 = new File(filePath2)
        FileWriter fileWriter = new FileWriter(outputFilePath)

        file1.eachLine { line ->
            fileWriter.write(line + "\n")
        }

        file2.eachLine { line ->
            fileWriter.write(line + "\n")
        }

        fileWriter.close()
        println("The fasta has been successfully created in : $outputFilePath")
    } catch (Exception e) {
        println("Error during the process : " + e.message)
    }
}


print "Please enter the .fasta file containing the human genome: "
def humanGenome = System.console().readLine()
print "Please choose the .fasta file containing the HIV genome: "
def hivGenome = System.console().readLine()
print "Please enter the name of the chimeric genome you want to create (without .fa)"
def name= System.console().readLine()
print "Please choose the directory where you want to save the chimeric genome (ending with /)"
def dir=System.console().readLine()

def finalName=dir+name+".fa"

concatenateFiles(humanGenome,hivGenome,finalName)

def indexName=finalName.substring(0,finalName.length()-2)+"mni"

def command="minimap2 -I2g -d "+indexName+" "+finalName

ProcessBuilder processBuilder = new ProcessBuilder()
processBuilder.command("bash", "-c", command)


processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT)


Process process = processBuilder.start()


int exitCode = process.waitFor()


if (exitCode == 0) {
    println "The indexing has been succesfully fulfiled in: $indexName"
} else {
    println "Indexing failed: $exitCode"
}


